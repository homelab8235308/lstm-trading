# Utilise l'image de base nginx
FROM nginx:alpine

# Copie la page HTML dans le répertoire par défaut de nginx
COPY index.html /usr/share/nginx/html/

# Expose le port 8880
EXPOSE 8880

# Commande par défaut pour lancer nginx
CMD ["nginx", "-g", "daemon off;"]
